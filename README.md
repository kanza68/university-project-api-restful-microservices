# University project - API RESTful Microservices

## Sujet
Ce projet était un projet Universitaire qui consistant en la réalisation d'une API métier permettant la gestion d’offres de stages, de la publication jusqu’au recrutement.

## Objectifs
Les objectifs principaux du projet étaient les suivants:
* développer une API RESTful en utilisant la technologie vue en cours (Spring boot)
* mettre en pratique HATEOAS (liens recherche-sélection, liens sélection-recrutement…)
* mettre en oeuvre des tests de votre API
* développer au moins deux services (personnes et offres) pour démontrer l’échange entre 2 services distincts.
