package org.miage.candidatureservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;

@SpringBootApplication
public class CandidatureServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CandidatureServiceApplication.class, args);
	}

	@Bean
	public OpenAPI candidatureAPI() {
		return new OpenAPI().info(new Info()
			.title("Candidature API")
			.version("1.0")
			.description("Documentation de l'API Candidature"));
	}
}
