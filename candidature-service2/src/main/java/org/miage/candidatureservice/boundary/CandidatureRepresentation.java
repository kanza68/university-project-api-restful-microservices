package org.miage.candidatureservice.boundary;

import java.net.URI;
import java.util.Optional;
import java.util.UUID;
import org.miage.candidatureservice.entity.*;
import javax.transaction.Transactional;

import org.miage.candidatureservice.control.CandidatureAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@Controller
@ResponseBody
@RequestMapping(value = "/candidatures", produces = MediaType.APPLICATION_JSON_VALUE)
public class CandidatureRepresentation {

	private final CandidatureResource ir;
	private final CandidatureAssembler ia;

	public CandidatureRepresentation(CandidatureResource ir, CandidatureAssembler ia) {
		this.ir = ir;
		this.ia = ia;
	}

	// GET all
	@GetMapping
	public ResponseEntity<?> getAllCandidatures() {
		return ResponseEntity.ok(ia.toCollectionModel(ir.findAll()));
	}

	// GET one
	@GetMapping(value = "/{candidatureId}")
	public ResponseEntity<?> getCandidatureById(@PathVariable("candidatureId") String id) {
		Optional<Candidature> optionalCandidature = ir.findById(id);
		if (optionalCandidature.isPresent()) {
			Candidature candidature = optionalCandidature.get();
			String idCandidat = candidature.getId();
			return ResponseEntity.ok(ia.toModel(candidature));
		} else {
			return ResponseEntity.notFound().build();
		}
	}



	// GET all by Id candidat
	@GetMapping(value = "/candidat/{idCandidat}")
	public ResponseEntity<?> getAllCandidaturesByIdCandidat(@PathVariable("idCandidat") String idCandidat) {
		Iterable<Candidature> candidatures = ir.findAll();
		CollectionModel<EntityModel<Candidature>> collectionModel = ia.toCollectionModelByCandidat(candidatures, idCandidat);
		return ResponseEntity.ok(collectionModel);
	}

	// GET all by Id offre
	@GetMapping(value = "/offre/{idOffre}")
	public ResponseEntity<?> getAllCandidaturesByIdOffre(@PathVariable("idOffre") String idOffre) {
		Iterable<Candidature> candidatures = ir.findAll();
		CollectionModel<EntityModel<Candidature>> collectionModel = ia.toCollectionModelByOffre(candidatures, idOffre);
		return ResponseEntity.ok(collectionModel);
	}

	// POST
	@PostMapping
	@Transactional
	public ResponseEntity<?> save(@RequestBody Candidature candidature) {
		Candidature toSave = new Candidature(UUID.randomUUID().toString(),
				candidature.getDatecandidature(),
				candidature.getStatut(),
				candidature.getIdcandidat(),
				candidature.getIdoffre());
		Candidature saved = ir.save(toSave);
		
		URI location = linkTo(CandidatureRepresentation.class).slash(saved.getId()).toUri();
		return ResponseEntity.created(location).build();
	}

	// DELETE
	@DeleteMapping(value = "/{candidatureId}")
	@Transactional
	public ResponseEntity<?> delete(@PathVariable("candidatureId") String id) {
		Optional<Candidature> candidature = ir.findById(id);
		candidature.ifPresent(ir::delete);
		return ResponseEntity.noContent().build();
	}


	@PutMapping(value = "/{candidatureId}")
	@Transactional
	public ResponseEntity<?> update(@PathVariable("candidatureId") String id,
			@RequestBody Candidature newCandidature) {
		Optional<Candidature> optionalCandidature = ir.findById(id);
		if (!optionalCandidature.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		Candidature candidature = optionalCandidature.get();
		candidature.setDatecandidature(newCandidature.getDatecandidature());
		candidature.setStatut(newCandidature.getStatut());
		candidature.setIdcandidat(newCandidature.getIdcandidat());
		candidature.setIdoffre(newCandidature.getIdoffre());
		ir.save(candidature);
		return ResponseEntity.ok().build();
	}

	// Abandonner une candidature 
	@PutMapping("/{candidatureId}/abandonner")
	public ResponseEntity<Void> abandonnerCandidature(@PathVariable("candidatureId") String id
			,@RequestBody Candidature newCandidature) {
		Optional<Candidature> optionalCandidature = ir.findById(id);
		if (optionalCandidature.isPresent()) {
			Candidature candidature = optionalCandidature.get();
			candidature.setStatut("Abandonnée");
			ir.save(candidature);
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	// GET All offre
	@GetMapping("/offres")
	public ResponseEntity<String> getAllOffreWithCandidatures() {
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8082/offres/";
		HttpEntity<String> requestEntity = new HttpEntity<>(null);
		ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);
		String response = responseEntity.getBody();
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	// GET offre
	@GetMapping("/offres/{id}")
	public ResponseEntity<String> getOffreWithCandidatures(@PathVariable Long id) {
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8082/offres/" + id;
		HttpEntity<String> requestEntity = new HttpEntity<>(null);
		ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);
		String response = responseEntity.getBody();
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	// GET offre by ID Candidature
	@GetMapping("/{candidatureId}/offres")
	public ResponseEntity<String> getOffreByIdCandidature(@PathVariable("candidatureId") String candidatureId) {
		Optional<Candidature> optionalCandidature = ir.findById(candidatureId);
		if (optionalCandidature.isPresent()) {
			Candidature candidature = optionalCandidature.get();
			String idOffre = candidature.getIdoffre();
			ResponseEntity<String> responseEntity = getOffreWithCandidatures(Long.valueOf(idOffre));
			return new ResponseEntity<>(responseEntity.getBody(), HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

}