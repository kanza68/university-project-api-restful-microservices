package org.miage.candidatureservice.boundary;

import java.util.List;
import java.util.Optional;

import org.miage.candidatureservice.entity.Candidature;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CandidatureResource extends JpaRepository<Candidature, String> {
	
	// JPA va fournir les SELECT, INSERT, UPDATE, DELETE
    
}
