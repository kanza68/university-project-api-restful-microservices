package org.miage.candidatureservice.control;

import java.util.List;

import org.miage.candidatureservice.boundary.CandidatureRepresentation;
import org.miage.candidatureservice.entity.Candidature;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;
import java.util.stream.StreamSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@Component
public class CandidatureAssembler implements RepresentationModelAssembler<Candidature, EntityModel<Candidature>> {

    @Override
    public EntityModel<Candidature> toModel(Candidature candidature) {
        return EntityModel.of(candidature,
                linkTo(methodOn(CandidatureRepresentation.class)
                    .getCandidatureById(candidature.getId())).withSelfRel(),
                linkTo(methodOn(CandidatureRepresentation.class)
                    .getAllCandidatures()).withRel("collection"));
    }

    @Override
    public CollectionModel<EntityModel<Candidature>> toCollectionModel(Iterable<? extends Candidature> entities) {
        List<EntityModel<Candidature>> intervenanModel = StreamSupport
            .stream(entities.spliterator(), false)
            .map(this::toModel)
            .toList();

        return CollectionModel.of(intervenanModel, linkTo(methodOn(CandidatureRepresentation.class)
            .getAllCandidatures()).withSelfRel());
    }
    
    // Permet de récupérer la liste des candidature à partir de l'id d'un candidat
    public CollectionModel<EntityModel<Candidature>> toCollectionModelByCandidat(Iterable<? extends Candidature> entities, String idCandidat) {
        List<EntityModel<Candidature>> candidatureModel = StreamSupport
                .stream(entities.spliterator(), false)
                .filter(candidature -> candidature.getIdcandidat().equals(idCandidat))
                .map(this::toModel)
                .toList();

        return CollectionModel.of(candidatureModel, linkTo(methodOn(CandidatureRepresentation.class)
                .getAllCandidatures()).withSelfRel());
    }
    
    // Permet de récupérer la liste des candidature à partir de l'id d'une offre
    public CollectionModel<EntityModel<Candidature>> toCollectionModelByOffre(Iterable<? extends Candidature> entities, String idOffre) {
        List<EntityModel<Candidature>> candidatureModel = StreamSupport
                .stream(entities.spliterator(), false)
                .filter(candidature -> candidature.getIdoffre().equals(idOffre))
                .map(this::toModel)
                .toList();

        return CollectionModel.of(candidatureModel, linkTo(methodOn(CandidatureRepresentation.class)
                .getAllCandidatures()).withSelfRel());
    }
}