package org.miage.candidatureservice.entity;

public class Adresse {

	private int idAdresse;
	private String adresseRue;
	private String adressePays;
	private String adresseVille;
	private String codePostal;
	private String lieuStage;
	private String telephone;
	private String url;
	private double latitude;
	private double longitude;

}
