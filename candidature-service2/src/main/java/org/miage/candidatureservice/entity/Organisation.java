package org.miage.candidatureservice.entity;

import java.io.Serial;

import javax.persistence.Id;

public class Organisation {
	  @Serial
	    private static final long serialVersionUID = 98765432345678L;
	    @Id
    private String organisation;
    private String adresse;
    private String adressePays;
    private String adresseVille;
    private String codePostal;
    private String adresseRue;
    private String email;
    private String telephone;
}
