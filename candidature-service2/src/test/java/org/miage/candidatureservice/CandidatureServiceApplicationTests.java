package org.miage.candidatureservice;


import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.miage.candidatureservice.boundary.CandidatureResource;
import org.miage.candidatureservice.entity.Candidature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.server.LocalServerPort;
import io.restassured.RestAssured;
import static io.restassured.RestAssured.when;
import static org.hamcrest.CoreMatchers.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class CandidatureServiceApplicationTests {

	@LocalServerPort
	int port;

	@Autowired
	CandidatureResource ir;

	@BeforeEach
	public void setupContext() {
		ir.deleteAll();
		RestAssured.port =port;
	}

	@Test
	void ping () {
		when().get("/candidatures").then().statusCode(HttpStatus.SC_OK);
	}

	@Test
	void getAll() {
		Candidature c2 = new Candidature(UUID.randomUUID().toString(),
				"2023-03-29", "Accepté", "2", "1");
		ir.save(c2);
		Candidature c3 = new Candidature(UUID.randomUUID().toString(),
				"2023-03-27", "En attente", "1", "1");
		ir.save(c3);
		Candidature c4 = new Candidature(UUID.randomUUID().toString(),
				"2023-03-26", "Accepté", "2", "3");
		ir.save(c4);
		when().get("/candidatures").then().statusCode(HttpStatus.SC_OK)
		.and().assertThat().body("size()",equalTo(2));
	}
}
