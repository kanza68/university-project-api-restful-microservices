package org.miage.api.api_offre;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;

@SpringBootApplication
public class OffreServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OffreServiceApplication.class, args);
	}
	@Bean
	public OpenAPI OffreAPI() {
		return new OpenAPI().info(new Info()
				.title("Offre API")
				.version("1.0")
				.description("Documentation de l'API Offre"));
	}
}
