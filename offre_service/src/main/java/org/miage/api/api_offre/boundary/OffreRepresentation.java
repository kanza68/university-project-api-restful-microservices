package org.miage.api.api_offre.boundary;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.miage.api.api_offre.control.OffreAssembler;
import org.miage.api.api_offre.entity.*;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@Controller
@ResponseBody
@RequestMapping(value="/offres", produces=MediaType.APPLICATION_JSON_VALUE)
public class OffreRepresentation {

	private final OffreResource ir;
	private final OffreAssembler ia;


	public OffreRepresentation(OffreResource ir, OffreAssembler ia) {
		this.ir = ir;
		this.ia = ia; 
	}

	// GET all
	@GetMapping
	public ResponseEntity<?> getAllOffres() {
		return ResponseEntity.ok(ia.toCollectionModel(ir.findAll()));
	}

	// GET one
	@GetMapping(value="/{offreId}")
	public ResponseEntity<?> getOffreById(@PathVariable("offreId") String id){
		return Optional.of(ir.findById(id))
				.filter(Optional::isPresent)
				.map(i -> ResponseEntity.ok(i.get()))
				.orElse(ResponseEntity.notFound().build());
	}
	
	// GET offres by params
	@GetMapping(value="/research")
	public ResponseEntity<?> getOffresByParams(
	        @RequestParam(required = false) String domaine,
	        @RequestParam(required = false) String dateDebut
	        ) {

	    List<Offre> offres = ir.findAll();

	    // Filtrage sur la liste d'objets Offre
	    if (domaine != null) {
	        offres = offres.stream().filter(offre -> offre.getDomaine() .equals(domaine)).collect(Collectors.toList());
	    }
	    if (dateDebut != null) {
	        offres = offres.stream().filter(offre -> offre.getDatedebutstage().equals(dateDebut)).collect(Collectors.toList());
	    }

	    return ResponseEntity.ok(ia.toCollectionModel(offres));
	}

	
	// POST - Ajout d'une offre
	@PostMapping
	@Transactional
	public ResponseEntity<?> save(@RequestBody Offre offre) {
		Offre toSave = new Offre(UUID.randomUUID().toString(),
				"vacante",
				offre.getNomstage(),
				offre.getDomaine(),
				offre.getDescriptionstage(),
				offre.getNiveauetudesstage(),
				offre.getExperiencesequisestage(),
				offre.getDatedebutstage(),
				offre.getDureestage(),
				offre.getSalairestage(),
				offre.getIndemnisation());
		Offre saved = ir.save(toSave);

		URI location = linkTo(OffreRepresentation.class).slash(saved.getId()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	// PUT - Mettre à jour une offre
	@PutMapping(value = "/{offreId}")
	@Transactional
	public ResponseEntity<?> updateOffre(@PathVariable("offreId") String id, @RequestBody Offre updatedOffre) {
	    // Vérifier si l'offre existe ou non
	    Offre existingOffre = ir.findById(id).orElse(null);
	    if (existingOffre == null) {
	        return ResponseEntity.notFound().build();
	    }
	    // Mettre à jour les champs de l'offre existante
	    existingOffre.setStatus(updatedOffre.getStatus());
	    existingOffre.setNomstage(updatedOffre.getNomstage());
	    existingOffre.setDomaine(updatedOffre.getDomaine());
	    existingOffre.setDescriptionstage(updatedOffre.getDescriptionstage());
	    existingOffre.setNiveauetudesstage(updatedOffre.getNiveauetudesstage());
	    existingOffre.setExperiencesequisestage(updatedOffre.getExperiencesequisestage());
	    existingOffre.setDatedebutstage(updatedOffre.getDatedebutstage());
	    existingOffre.setDureestage(updatedOffre.getDureestage());
	    existingOffre.setSalairestage(updatedOffre.getSalairestage());
	    existingOffre.setIndemnisation(updatedOffre.getIndemnisation());
	    // Enregistrer les modifications
	    ir.save(existingOffre);
	    return ResponseEntity.ok().build();
	}


	// DELETE 
	@DeleteMapping(value="/{offreId}")
	@Transactional
	public ResponseEntity<?> deleteOffreById(@PathVariable("offreId") String id)
	{ 
		if(ir.existsById(id)){ 
			ir.deleteById(id); 
			return ResponseEntity.noContent().build(); 
		} 
		else { 
			return ResponseEntity.notFound().build(); 
		} 
	}

	// GET ALL Candidatures
	@GetMapping("/candidatures")
	public ResponseEntity<String> getAllCandidatures() {
	    RestTemplate restTemplate = new RestTemplate();
	    String url = "http://localhost:8081/candidatures/";
	    HttpEntity<String> requestEntity = new HttpEntity<>(null);
	    ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);
	    String response = responseEntity.getBody();

	    return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	// GET ONE Candidature by ID
	@GetMapping("/candidatures/{id}")
	public ResponseEntity<String> getCandidaturesById(@PathVariable Long id) {
	    RestTemplate restTemplate = new RestTemplate();
	    String url = "http://localhost:8081/candidatures/" + id;
	    HttpEntity<String> requestEntity = new HttpEntity<>(null);
	    ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);
	    String response = responseEntity.getBody();

	    return new ResponseEntity<>(response, HttpStatus.OK);
	}

	
	// GET ONE candidatures by ID offre
	@GetMapping("/{id}/candidatures")
	public ResponseEntity<String> getCandidaturesByIdOffre(@PathVariable Long id) {
	    RestTemplate restTemplate = new RestTemplate();
	    String url = "http://localhost:8081/candidatures/offre/" + id;
	    HttpEntity<String> requestEntity = new HttpEntity<>(null);
	    ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);
	    String response = responseEntity.getBody();

	    return new ResponseEntity<>(response, HttpStatus.OK);
	}

}