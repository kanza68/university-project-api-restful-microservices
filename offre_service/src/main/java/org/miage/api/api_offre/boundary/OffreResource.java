package  org.miage.api.api_offre.boundary;

import org.miage.api.api_offre.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OffreResource extends JpaRepository<Offre, String> {

		// JPA va fournir les select insert update delete etc...
		// permet à écrire des requete sans écrire des requete (génère automatique) on a pas écrire des SQL. Spring va prendre methode et va généré SQl
		
		// Exemple select intervenant par code postal
		//List<Offre> findById(String id);
    
}