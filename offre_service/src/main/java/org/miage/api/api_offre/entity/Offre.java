package org.miage.api.api_offre.entity;

import java.io.Serial;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor          // obligatoire si JPA
@Data
public class Offre {
	
	@Serial
	private static final long serialVersionUID = 98765432345678L;
	
	@Id
	private String id;
	@Column(columnDefinition = "VARCHAR(255) DEFAULT 'vacante'")
    private String status;
	private String nomstage;
	private String domaine;
	private String descriptionstage;
	private String niveauetudesstage;
	private String experiencesequisestage;
	private String datedebutstage;
	private String dureestage;
	private String salairestage;
	private String indemnisation;
	//private Organisation organisation; 
}
