package org.miage.api.api_offre.entity;

import java.io.Serial;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor          // obligatoire si JPA
@Data
public class Organisation {
	  @Serial
	    private static final long serialVersionUID = 98765432345678L;
	  
	  @Id
	  private String id;
	  private String adresse;
	  private String adressePays;
	  private String adresseVille;
	  private String codePostal;
	  private String adresseRue;
	  private String email;
	  private String telephone;
	  private String url;
}
