package org.miage.api.api_offre;


import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.miage.api.api_offre.boundary.OffreResource;
import org.miage.api.api_offre.entity.Offre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.server.LocalServerPort;
import io.restassured.RestAssured;
import static io.restassured.RestAssured.when;
import static org.hamcrest.CoreMatchers.*;
import java.util.UUID;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class OffreServiceApplicationTests {

    @LocalServerPort
    int port;

    @Autowired
    OffreResource ir;

    @BeforeEach
    public void setupContext() {
        ir.deleteAll();
        RestAssured.port =port;
    }

    @Test
    void ping () {
        when().get("/offre").then().statusCode(HttpStatus.SC_OK);
    }

    @Test
    void getAll() {
        Offre i1 = new Offre(UUID.randomUUID().toString(), "Stage en marketing", "Marketing", "Nous recherchons un stagiaire pour nous aider à développer notre stratégie marketing", "Bac+3", "Aucune expérience requise", "2023-06-01", "6 mois", "1000", "50% de reduction carte bus, tickets restau");
        ir.save(i1);
        Offre i2 = new Offre(UUID.randomUUID().toString(), "Stage en marketing", "Marketing", "Nous recherchons un stagiaire pour nous aider à développer notre stratégie marketing", "Bac+3", "Aucune expérience requise", "2023-06-01", "6 mois", "1000", "50% de reduction carte bus, tickets restau");
    ir.save(i2);
    Offre i3 = new Offre(UUID.randomUUID().toString(), "Stage en marketing", "Marketing", "Nous recherchons un stagiaire pour nous aider à développer notre stratégie marketing", "Bac+3", "Aucune expérience requise", "2023-06-01", "6 mois", "1000", "50% de reduction carte bus, tickets restau");
    ir.save(i3);
        when().get("/offres").then().statusCode(HttpStatus.SC_OK)
            .and().assertThat().body("size()",equalTo(2));
    }
}
