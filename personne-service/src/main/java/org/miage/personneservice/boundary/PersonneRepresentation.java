package org.miage.personneservice.boundary;

import java.net.URI;
import java.util.Optional;
import java.util.UUID;
import org.miage.personneservice.entity.*;
import javax.transaction.Transactional;
import org.miage.personneservice.control.PersonneAssembler;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@Controller
@ResponseBody
@RequestMapping(value = "/personnes", produces = MediaType.APPLICATION_JSON_VALUE)
public class PersonneRepresentation {

	private final PersonneResource ir;
	private final PersonneAssembler ia;

	public PersonneRepresentation(PersonneResource ir, PersonneAssembler ia) {
		this.ir = ir;
		this.ia = ia;
	}

	// GET all
	@GetMapping
	public ResponseEntity<?> getAllPersonnes() {
		return ResponseEntity.ok(ia.toCollectionModel(ir.findAll()));
	}

	// GET one
	@GetMapping(value = "/{personneId}")
	public ResponseEntity<?> getPersonneById(@PathVariable("personneId") String id) {
		return Optional.of(ir.findById(id))
				.filter(Optional::isPresent)
				.map(i -> ResponseEntity.ok(ia.toModel(i.get())))
				.orElse(ResponseEntity.notFound().build());
	}
	// DELETE
	@DeleteMapping(value = "/{personneId}")
	@Transactional
	public ResponseEntity<?> delete(@PathVariable("personneId") String id) {
		Optional<Personne> personne = ir.findById(id);
		personne.ifPresent(ir::delete);
		return ResponseEntity.noContent().build();
	}

	// GET All candidatures
	@GetMapping("/candidatures")
	public ResponseEntity<String> getAllOffreWithUsers() {
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8081/candidatures/";
		HttpEntity<String> requestEntity = new HttpEntity<>(null);
		ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);
		String response = responseEntity.getBody();
		return new ResponseEntity<>(response, HttpStatus.OK);
	}



	// GET candidatures de la personne
	@GetMapping("/{id}/candidatures")
	public ResponseEntity<String> getCandidaturesByPersonneId(@PathVariable("id") String personneId) {
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8081/candidatures/candidat/" + personneId;
		HttpEntity<String> requestEntity = new HttpEntity<>(null);
		ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);
		String response = responseEntity.getBody();
		return new ResponseEntity<>(response, HttpStatus.OK);
	}


	//POST
	@PostMapping
	@Transactional
	public ResponseEntity<?> save(@RequestBody Personne personne) {
		Personne toSave = new Personne(UUID.randomUUID().toString(),
				personne.getNom(),
				personne.getPrenom(),
				personne.getAge(),
				personne.getAdressepostal(),
				personne.getCodepostal(),
				personne.getAdressemail(),
				personne.getNiveauetude(),
				personne.getCv(),
				personne.getProfession(),
				personne.getTelephone(),
				personne.getUrl(),
				personne.getIdcandidature());
		Personne saved = ir.save(toSave);
		URI location = linkTo(PersonneRepresentation.class).slash(saved.getId()).toUri();
		return ResponseEntity.created(location).build();
	}



	//PUT
	@PutMapping(value = "/{personneId}")
	@Transactional
	public ResponseEntity<?> update(@PathVariable("personneId") String id,
			@RequestBody Personne newPersonne) {
		Optional<Personne> optionalPersonne = ir.findById(id);
		if (!optionalPersonne.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		Personne personne = optionalPersonne.get();
		personne.setNom(newPersonne.getNom());
		personne.setPrenom(newPersonne.getPrenom());
		personne.setAge(newPersonne.getAge());
		personne.setAdressepostal(newPersonne.getAdressepostal());
		personne.setCodepostal(newPersonne.getCodepostal());
		personne.setAdressemail(newPersonne.getAdressemail());
		personne.setNiveauetude(newPersonne.getNiveauetude());
		personne.setCv(newPersonne.getCv());
		personne.setProfession(newPersonne.getProfession());
		personne.setTelephone(newPersonne.getTelephone());
		personne.setUrl(newPersonne.getUrl());
		ir.save(personne);
		return ResponseEntity.ok().build();
	}


	// GET ALL Offres
	@GetMapping("/offres")
	public ResponseEntity<String> getAllOffres() {
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8082/offres/";
		HttpEntity<String> requestEntity = new HttpEntity<>(null);
		ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);
		String response = responseEntity.getBody();

		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}