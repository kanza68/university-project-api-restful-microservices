package org.miage.personneservice.boundary;


import org.miage.personneservice.entity.Personne;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonneResource extends JpaRepository<Personne, String> {

	// JPA va fournir les SELECT, INSERT, UPDATE, DELETE
	//List<Personne> findById(String id);

}
