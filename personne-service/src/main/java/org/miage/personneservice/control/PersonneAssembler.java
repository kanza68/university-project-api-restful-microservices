package org.miage.personneservice.control;

import java.util.List;

import org.miage.personneservice.boundary.PersonneRepresentation;
import org.miage.personneservice.entity.Personne;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;
import java.util.stream.StreamSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@Component
public  class PersonneAssembler implements RepresentationModelAssembler<Personne, EntityModel<Personne>> {

	@Override
	public EntityModel<Personne> toModel(Personne personne) {
		return EntityModel.of(personne,
				linkTo(methodOn(PersonneRepresentation.class)
						.getPersonneById(personne.getId())).withSelfRel(),
				linkTo(methodOn(PersonneRepresentation.class)
						.getAllPersonnes()).withRel("collection"));
	}

	@Override
	public CollectionModel<EntityModel<Personne>> toCollectionModel(Iterable<? extends Personne> entities) {
		List<EntityModel<Personne>> personnModel = StreamSupport
				.stream(entities.spliterator(), false)
				.map(this::toModel)
				.toList();

		return CollectionModel.of(personnModel, linkTo(methodOn(PersonneRepresentation.class)
				.getAllPersonnes()).withSelfRel());
	}
}