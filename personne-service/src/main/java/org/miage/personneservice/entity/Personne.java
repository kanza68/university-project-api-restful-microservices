package org.miage.personneservice.entity;

import java.io.Serial;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor          // obligatoire si JPA
@Data

public class Personne {
	@Serial
	private static final long serialVersionUID = 98765432345678L;
	@Id
	private String id;
	private String nom;
	private String prenom;
	private int age;
	private String adressepostal;
	private int codepostal;
	private String adressemail;
	private String niveauetude;
	private String cv;
	private String profession;
	private String telephone;
	private String url;
	private String idcandidature;
}
