INSERT INTO Personne (id, nom, prenom, age, adressepostal, codepostal, adressemail, niveauetude, cv, profession, telephone, url,idcandidature)
VALUES 
    ('1', 'Doe', 'John', 35, '12 rue de la Paix', 75001, 'johndoe@gmail.com', 'Bac+5', 'CV John Doe', 'Ing�nieur informatique', '0601020304', 'https://www.linkedin.com/in/johndoe/',1),
    ('2', 'Dubois', 'Marie', 28, '5 avenue des Champs-Elys�es', 75008, 'marie.dubois@hotmail.com', 'Bac+3', 'CV Marie Dubois', 'Marketing Manager', '0620304050', 'https://www.linkedin.com/in/mariedubois/',1),
    ('3', 'Martin', 'Sophie', 42, '23 rue du Faubourg Saint-Honor�', 75008, 'sophie.martin@yahoo.fr', 'Bac+4', 'CV Sophie Martin', 'Chef de projet', '0678901234', 'https://www.linkedin.com/in/sophiemartin/',3),
    ('4', 'Garcia', 'Juan', 31, '8 rue de Belleville', 75020, 'juangarcia@gmail.com', 'Bac+2', 'CV Juan Garcia', 'Vendeur', '0607080910', 'https://www.linkedin.com/in/juangarcia/',3),
    ('5', 'Smith', 'Emma', 25, '2 rue de la Libert�', 69003, 'emma.smith@hotmail.com', 'Bac+5', 'CV Emma Smith', 'Journaliste', '0678912345', 'https://www.linkedin.com/in/emmasmith/',1),
    ('6', 'Dupont', 'Paul', 47, '18 avenue des Ternes', 75017, 'paul.dupont@yahoo.fr', 'Bac+3', 'CV Paul Dupont', 'Directeur commercial', '0656789012', 'https://www.linkedin.com/in/pauldupont/',4),
    ('7', 'Leblanc', 'C�line', 29, '25 rue Saint-Antoine', 75004, 'celine.leblanc@gmail.com', 'Bac+4', 'CV C�line Leblanc', 'Consultante RH', '0678901234', 'https://www.linkedin.com/in/celineleblanc/',4),
    ('8', 'Rousseau', 'Antoine', 38, '7 rue de la R�publique', 69001, 'antoine.rousseau@gmail.com', 'Bac+5', 'CV Antoine Rousseau', 'D�veloppeur web', '0687654321', 'https://www.linkedin.com/in/antoinerousseau/',5),
    ('9', 'Petit', 'Marie', 27, '14 rue des Lilas', 69002, 'marie.petit@hotmail.com', 'Bac+3', 'CV Marie Petit', 'Chef de produit', '0623456789', 'https://www.linkedin.com/in/mariepetit/',5);