package org.miage.personneservice;


import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.miage.personneservice.boundary.PersonneResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.server.LocalServerPort;
import io.restassured.RestAssured;
import static io.restassured.RestAssured.when;
import static org.hamcrest.CoreMatchers.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class PersonneServiceApplicationTests {

	@LocalServerPort
	int port;

	@Autowired
	PersonneResource ir;

	@BeforeEach
	public void setupContext() {
		ir.deleteAll();
		RestAssured.port =port;
	}

	@Test
	void ping () {
		when().get("/personnes").then().statusCode(HttpStatus.SC_OK);
	}


}
